#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

#define TRUE 1
#define MAXSZ 100 // max number of commands' letters
#define MAXARG 20 // max number of arguments

// Clearing the shell using escape sequences 
#define clear() printf("\033[H\033[J")

struct bg_jobs {
	int id;
	struct bg_jobs *next;
};

char currentdir[1024];
int isdaemon = 0;
struct bg_jobs *mydeamons;

// Ctrl+C
void sigkill(pid_t pid){
	signal(SIGINT,&sigkill);  
	kill(pid,SIGKILL);

	fflush(stdout);
}

void type_prompt(){
	//clear();
	printf("%s@cs345sh%s/$ ", getlogin(), getcwd(currentdir, sizeof(currentdir)));
}

int read_command(char *command){

	fgets(command, MAXSZ, stdin);
	command[strlen(command)-1] = '\0'; // \n -> \0

	if (strlen(command) != 0) return 0; 
	else return 1; 
}

void storeInBackground(int pid){

	struct bg_jobs *tmp, *prev, *new;

	tmp = mydeamons;
	new = (struct bg_jobs*)malloc(sizeof(struct bg_jobs));
	new->id = pid;
	if(tmp == NULL) {
		mydeamons = new;
	} else {

		while(tmp != NULL) {
			prev = tmp;
			tmp = tmp->next;
		} /* prev points last node */

		/* insert node to the list */
		prev->next = new;
	}
}

/* the system command is executed here */
void execArgs(char** parsed){ 

	int i, status;
    // Forking a child 
    pid_t pid = fork();  
  
    if (pid == -1) { 
    	printf("\nError: creation of child process was unsuccessful\n");
    	return; 
    } else if (pid == 0) {

    	signal(SIGINT,&sigkill);
    	//while(1);

    	if (execvp(parsed[0], parsed) < 0) { 
    		printf("Execution failed\n"); 
    	} 
    	exit(0);
    } else { 
    	/* process in background */
    	if(isdaemon){
    		isdaemon = 0;
    		storeInBackground(pid);
    		for(i=0; i< sizeof(*parsed); i++){
    			if(!parsed[i]) break;
    			printf("%s ", parsed[i]);
    		}
    		printf(", PID: %d\n", pid);
    		return;
    	}
        waitpid(pid, &status, 0); /* wait for child to exit */
    	return; 
    }
}

void execArgsRedir(char** parsed, char** parsed_redir, char* symbol){

    int fd, i, j, status;
    char *file, *cmd;
    pid_t pid = fork();

    if(strcmp(symbol, ">")==0){

    	file = parsed_redir[1];
    	cmd = parsed[0];

    	/* command with parameters only */
    	for (i = 0; i < MAXARG; i++){
    		if(parsed[i][0]=='>'){
    			parsed[i] = NULL;
    			break;
    		}
    	}   	
    	if (pid == 0) { 
	    	/* Child process */
    		fd = open(file, O_WRONLY|O_TRUNC|O_CREAT, S_IRWXU);
    		dup2(fd, STDOUT_FILENO); 
    		close(fd);

    		if (execvp(cmd, parsed) < 0) { 
    			printf("\nCould not execute command\n"); 
    			exit(0); 
    		} 
    	} else {
		/* Parent process */
    		if(isdaemon) {
    			isdaemon = 0;
    			storeInBackground(pid);
    			for(i=0; i< sizeof(*parsed); i++){
    				if(!parsed[i]) break;
    				printf("%s ", parsed[i]);
    			}
    			printf(", PID: %d\n", pid);
    			return;
    		}
    		waitpid(pid, &status, 0);
    	}
    /************************************************************/
	} else if(strcmp(symbol, ">>")==0) {

		file = parsed_redir[1];
		cmd = parsed[0];
		/* command with parameters only */
    	for (i = 0; i < MAXARG; i++){
    		if(parsed[i][0]=='>'){
    			parsed[i] = NULL;
    			break;
    		}
    	}
    	if (pid == 0) { 	        
	    	/* Child process */
			fd = open(file, O_WRONLY|O_APPEND|O_CREAT, S_IRWXU);
			dup2(fd, STDOUT_FILENO); 
			close(fd);

	        if (execvp(cmd, parsed) < 0) { 
	            printf("\nCould not execute command\n"); 
	            exit(0); 
	        } 
	    } else {
		/* Parent process */
	    	if(isdaemon) {
    			isdaemon = 0;
    			storeInBackground(pid);
    			for(i=0; i< sizeof(*parsed); i++){
    				if(!parsed[i]) break;
    				printf("%s ", parsed[i]);
    			}
    			printf(", PID: %d\n", pid);
    			return;
    		}
			waitpid(pid, &status, 0);
		}
	/************************************************************/
	} else if(strcmp(symbol, "<")==0) {

		file = parsed_redir[1];
		cmd = parsed[0];

		/* command with parameters only */
    	for (i = 0; i < MAXARG; i++){
    		if(parsed[i][0]=='<'){
    			parsed[i] = NULL;
    			break;
    		}
    	}
		if (pid == 0) { 
	    	/* Child process */
			fd = open(file, O_RDONLY);
			dup2(fd, STDIN_FILENO);
			close(fd);

			if (execvp(cmd, parsed) < 0) { 
				printf("\nCould not execute command\n"); 
				exit(0); 
			} 
		} else {
		/* Parent process */
			if(isdaemon) {
    			isdaemon = 0;
    			storeInBackground(pid);
    			for(i=0; i< sizeof(*parsed); i++){
    				if(!parsed[i]) break;
    				printf("%s ", parsed[i]);
    			}
    			printf(", PID: %d\n", pid);
    			return;
    		}
			waitpid(pid, &status, 0);
		}
	}

	return;
}

/* detects symbols in a parsed string */
int redirectionSymbol(char** parsed, char** symbol){

	int i=0;
	*symbol = (char*)malloc(3*sizeof(char));
	while(parsed[i]!=NULL){
		if(strcmp(parsed[i], ">")==0){
			strcpy(symbol[0], ">");
			return 1;
		}
		if(strcmp(parsed[i], ">>")==0){
			strcpy(symbol[0], ">>");
			return 1;
		}
		if(strcmp(parsed[i], "<")==0){
			strcpy(symbol[0], "<");
			return 1;
		}
		i++;
	}
	return 0;
}

/* the piped system commands are executed here */
void execArgsPiped(char** parsed1, char** parsed2){
    // 0 is read end, 1 is write end 
    int pipefd[2], i, status, filefd;
    char *symbol, *file;
    pid_t p1, p2; 
  
    if (pipe(pipefd) < 0) { 
        printf("\nPipe could not be initialized\n"); 
        return; 
    }
    /*************************************************/
    p1 = fork(); 
    if (p1 < 0) { 
        printf("\nCould not fork"); 
        return; 
    } 

    if (p1 == 0) { 
        // Child 1 executing..
        // It only needs to write at the write end
        close(pipefd[0]); // close read end
        if(redirectionSymbol(parsed1, &symbol)){
        	i = 0;
        	while(strcmp(parsed1[i], symbol)!=0) i++;
        	file = parsed1[i+1];
        	parsed1[i] = NULL;

        	if(strcmp(symbol, ">")==0){
        		filefd = open(file, O_WRONLY|O_TRUNC|O_CREAT, S_IRWXU);
        		dup2(filefd, STDOUT_FILENO);
        	} else if(strcmp(symbol, ">>")==0){
        		filefd = open(file, O_WRONLY|O_APPEND|O_CREAT, S_IRWXU);
        		dup2(filefd, STDOUT_FILENO);
        	} else if(strcmp(symbol, "<")==0){
        		filefd = open(file, O_RDONLY);
        		dup2(filefd, STDIN_FILENO);
        	}
        	close(filefd);
        }

        dup2(pipefd[1], STDOUT_FILENO); 
        close(pipefd[1]);

        if (execvp(parsed1[0], parsed1) < 0) { 
        	printf("\nCould not execute command 1..\n"); 
        	exit(0); 
        } 
    }
    /*************************************************/
    else { 
        // Parent executing 
        p2 = fork(); 
  
        if (p2 < 0) { 
            printf("\nCould not fork"); 
            return; 
        }
        // Child 2 executing.. 
        // It only needs to read at the read end 
        if (p2 == 0) { 

            close(pipefd[1]); // close write end
            if(redirectionSymbol(parsed2, &symbol)){
            	i = 0;
            	while(strcmp(parsed2[i], symbol)!=0) i++;
            	file = parsed2[i+1];
            	parsed2[i] = NULL;

            	if(strcmp(symbol, ">")==0){
            		filefd = open(file, O_WRONLY|O_TRUNC|O_CREAT, S_IRWXU);
            		dup2(filefd, STDOUT_FILENO);
            	} else if(strcmp(symbol, ">>")==0){
            		filefd = open(file, O_WRONLY|O_APPEND|O_CREAT, S_IRWXU);
            		dup2(filefd, STDOUT_FILENO);
            	} else if(strcmp(symbol, "<")==0){
            		filefd = open(file, O_RDONLY);
            		dup2(filefd, STDIN_FILENO);
            	}
            	close(filefd);
            }

            dup2(pipefd[0], STDIN_FILENO); 
            close(pipefd[0]);

            if (execvp(parsed2[0], parsed2) < 0) { 
                printf("\nCould not execute command 2..\n"); 
                exit(0);
            } 
        } else { 
           /* parent executing, waiting for two children */
            close(pipefd[1]); 
            if(isdaemon) {
    			isdaemon = 0;
    			storeInBackground(p2);
    			for(i=0; i< sizeof(*parsed1); i++){
    				if(!parsed1[i]) break;
    				printf("%s ", parsed1[i]);
    			}
    			printf(", PID: %d\n", p1);
    			for(i=0; i< sizeof(*parsed2); i++){
    				if(!parsed2[i]) break;
    				printf("%s ", parsed2[i]);
    			}
    			printf(", PID: %d\n", p2);
    			return;
    		}
    		/* wait both children to exit */
            waitpid(p1, &status, 0);
            waitpid(p2, &status, 0);
        } 
    } 
}

void openHelp() {

    puts("\n* Linux C shell Help *"
        "\nList of Commands and Tasks supported:"
        "\n>cd, ls etc. (all the commands from ls /bin)"
        "\n>redirection"
        "\n>pipelining"
        "\n>pipe & redirection"
        "\n>background processes"
        "\n>exit\n");

    return; 
}

pid_t plookup(){
	struct bg_jobs *tmp, *prev;
	tmp = mydeamons;
	if(!tmp){
		printf("No background processes..\n");
		return 0;
	}
	if(tmp->next == NULL){
		mydeamons = NULL;

	} else {
		while(tmp->next != NULL){
			prev = tmp;
			tmp = tmp->next;
		} 
		if(prev) prev->next = NULL;
	}
	return tmp->id;
}

/* Executes builtin commands, handles the others */
int isBuiltin(char** parsed){ 

    int i, num = 4, check = 0;
    char* builtin[num];
    pid_t pid;
  
    builtin[0] = "exit"; 
    builtin[1] = "cd"; 
    builtin[2] = "fg";
    builtin[3] = "help"; 
  
    for (i = 0; i < num; i++) { 
        if (strcmp(parsed[0], builtin[i]) == 0) { 
            check = i + 1; 
            break; 
        } 
    }   
    switch (check) { 
    case 1:
        exit(0); 
    case 2:
        chdir(parsed[1]); 
        return 1; 
    case 3:
    	pid = plookup();
    	kill(pid, SIGCONT);
    	return 1;
    case 4:
        openHelp(); 
        return 1; 
    default: 
        return 0; 
    }  
}

void removeSpaceAfter(char* str, char symbol){

	int i, j, spaces = 0;

	/* temporary command: remove the space after pipe */
    for(i=0; i < strlen(str); i++){
    	if(/* (tmp_com[i]==' '&& tmp_com[i+1]=='|') || */
    	   (str[i]==' '&& str[i-1] == symbol)){

    		spaces++; // beware: spaces = 0

    		for(j=i; j < strlen(str)-spaces; j++){
    			str[j] = str[j+1];
    		}
    		str[j] = '\0';
    	}
    }
    return;
}

/* break up commands to individual words */
void parseSpaces(char* command, char** parsed){ 

    int i;
    char *str, *tmp_com;

    tmp_com = (char *)malloc(strlen(command));
    strcpy(tmp_com, command);

    for (i = 0; i < MAXARG; i++) {

    	str = strsep(&tmp_com, " "); // parse to spaces
    	if(str == NULL){
    		parsed[i] = NULL;
    		break;
    	} 
    	parsed[i] = (char *)malloc(strlen(str)*sizeof(char));

        strcpy(parsed[i], str);
        //printf("arg %d parsed: %s\n", i+1, parsed[i]);

        //if (parsed[i] == NULL) break;
        if (strlen(parsed[i]) == 0) i--;
    } 
}

/* break up piped command in two separate commands */
int parsePipe(char* command, char** strpiped){

    int i, spaces = 0;
    char *str, *tmp_com;

    tmp_com = (char *)malloc(strlen(command));

    strcpy(tmp_com, command);

    /* temporary command: remove the space after pipe */
    removeSpaceAfter(tmp_com, '|');

    for (i = 0; i < 2; i++) {

    	str = strsep(&tmp_com, "|");
    	if(str == NULL){
    		strpiped[i] = NULL;
    		break;
    	}
    	strpiped[i] = (char *)malloc(strlen(str)*sizeof(char));
    	strcpy(strpiped[i], str);
    } 
  
    if (strpiped[1] == NULL) 
        return 0; /* returns 0 if no pipe is found */
    else {
        return 1; 
    }
}

/* checks if the command includes redirection and stores the
   redirection symbol */
int redirectionCheck(char* command, char* symbol){

	int i;
	for(i=0; i<strlen(command); i++){

    	switch(command[i]){
    		case '>':
    			if(command[i+1]=='>'){
    				strcpy(symbol, ">>");
    				return 1;
    			} else {
    				strcpy(symbol, ">");
    				return 1;
    			}
    		case '<':
    			strcpy(symbol, "<");
    			return 1;
    		default: continue;
    	}
    }
    return 0;
}

/* separate command from redirecting file */
int parseRedirect(char* command, char** stredirect){

	int i;
    char *str, *tmp_com, *symbol;

    tmp_com = (char *)malloc(strlen(command));
    strcpy(tmp_com, command);

    symbol = (char *)malloc(3*sizeof(char));
    if(!redirectionCheck(command, symbol)) return 0;

    /* remove the space after redirection */
    removeSpaceAfter(tmp_com, symbol[0]);

    for (i = 0; i < 3; i++) {

    	str = strsep(&tmp_com, symbol);

    	if(strcmp(symbol, ">>")==0 && (tmp_com == NULL)){
    			stredirect[i-1] = (char *)malloc(strlen(str)*sizeof(char));
    			strcpy(stredirect[i-1], str);
    			break;
    	}

    	if(str == NULL){
    		stredirect[i] = NULL;
    		break; 		
    	}
    	stredirect[i] = (char *)malloc(strlen(str)*sizeof(char));
    	strcpy(stredirect[i], str);
    } 
    return 2;
}

int processCommand(char* command, char** parsed, char** parsed_pipe, char** redirected){ 

    char* strpiped[2];
    char* stredirect[2], *symbol;
    int pipe = 0, redirection = 0;
    int i, sz;

    int last = strlen(command)-1;
	if(command[last] == '&'){
		isdaemon = 1;
		command[last] = '\0';
	}

    redirection = parseRedirect(command, stredirect);

    if(redirection) {

    	for(i = 0; i < 2; i++){

    		sz = strlen(stredirect[i]);
    		redirected[i] = (char*)malloc(sz*sizeof(char));
    		strcpy(redirected[i], stredirect[i]);
    	}
    	redirected[i] = NULL;
    }

    pipe = parsePipe(command, strpiped); // pipe check + parse

    if(pipe) { 

    	if(redirection) redirection = 0; /* handle it on pipes */

    	parseSpaces(strpiped[0], parsed); // left instruction
    	parseSpaces(strpiped[1], parsed_pipe); // right instruction

    } else { 
    	parseSpaces(command, parsed);
    }

	if (isBuiltin(parsed)) 
		return 0; 
	else {
		return 1 + pipe + redirection;
	}
}

int main(){

	int flag = 0;
	char command[MAXSZ];
	char *parsed[MAXARG];
	char *symbol;
	char *parsed_pipe[MAXARG];
	char *parsed_redir[MAXARG];
	struct bg_jobs *tmp;

	while (TRUE) {               

		type_prompt(); /* display prompt on the screen */

		if (read_command(command)) /* read input from terminal */
			continue;

		flag = processCommand(command, parsed, parsed_pipe, parsed_redir);
        /* 0: no command or builtin command
           1: simple command 
           2: includes pipe
           3: redirected
		*/

		if (flag == 1) {
			execArgs(parsed);
		} else if (flag == 2) {
			execArgsPiped(parsed, parsed_pipe);
		} else if (flag == 3) {
			symbol = (char *)malloc(3*sizeof(char));
			redirectionCheck(command, symbol);
			execArgsRedir(parsed, parsed_redir, symbol);
		}
		/* tmp = mydeamons;
		printf("mydeamons:\n");
		while(tmp != NULL){
			printf("%d\n", tmp->id);
			tmp = tmp->next;
		}*/
	}
	return 0;
}