Marianna Velessioti, 3747

* Linux C Shell *
  List of Commands and Tasks supported:
     > cd, ls etc. (all the commands from ls /bin
     > redirection
     > pipelining
     > pipe & redirection
     > background processes
     > exit

  After a command is entered, the following things are done:

Parsing: breaking up of commands into individual words and strings
Checking for special characters like pipes, etc is done.
If command ends up with '&' sets it in the background.
Checking if built-in commands are asked for.
If redirections are present, handling redirections.
If pipes are present, handling pipes.
If pipes include redirections, handling them.
Executing system commands and libraries by forking a child and calling execvp.
Printing current directory name and asking for next input.

execution examples:
ls, ls -l -t, cat file.txt, sort file1, cat file1 > file2, sort < names | head -3,
ls | sort > out.txt, sleep 5, sleep 8 &, ls -la | head >> file, ./a.out < testfile,
cat file | grep pattern > outfile  etc.